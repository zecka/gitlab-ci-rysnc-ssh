Here's a summary of the steps you can follow:

## 1. Generate SSH keypair 
Generate ssh for your deployment. You can use the following command to generate an Ed25519 keypair:

```
# Linux or mac os
ssh-keygen -t ed25519 -C "my_deploy_key" -f ~/.ssh/my_deploy_key
# Windows
ssh-keygen -t ed25519 -C "my_deploy_key" -f %USERPROFILE%\.ssh\my_key
```
This will create a new SSH keypair named `my_deploy_key` in the `~/.ssh` directory.

## 2. Add public key to server authorized_keys

Copy the content of `~/.ssh/my_deploy_key.pub` and add it into to the authorized_keys file on your remote server.

Connect to your remote server using ssh and type following commands

- `mkdir ~/.ssh`
- `chmod 700 ~/.ssh`
- `touch ~/.ssh/authorized_keys`
- `echo YOUR_PUBLIC_KEY >> ~/.ssh/authorized_keys`
- `chmod 600 ~/.ssh/authorized_keys`

<details>
    <summary>How to copy file content trough command line</summary>

- Linux: `xclip -sel clip < ~/.ssh/my_deploy_key.pub`
- Mac OS: `pbcopy < ~/.ssh/my_deploy_key.pub`
- Windows: `type %USERPROFILE%\.ssh\my_deploy_key.pub | clip`

</details>




## 3. Define gitlab environnement variables
Add following variable to GitLab project's environment variables

| Variable name | Example value |
|--|--|
|SSH_PRIVATE_KEY| ssh-ed25519 AAAAC3NzaC1lZD...|
|LOCAL_PATH | /path/to/local/folder/ |
|REMOTE_PATH | /path/to/remote/folder/ |
|SSH_USER | user |
|SSH_HOST: | example.com |

<details>
  <summary>How to add gitlab variable </summary>

1. Go to your GitLab project's settings page, then select "CI/CD" in the sidebar.

2. Scroll down to the "Variables" section and click "Add variable".

3. Enter `SSH_PRIVATE_KEY` as the variable name.  
In the "Value" field, copy and paste the contents of your private key file (~/.ssh/my_deploy_key).

4. Set the "Type" dropdown to "File" (Or "Variable" fot others).

5. Click "Add variable" to save the variable.
</details>



## 4. Create .gitlab-ci.yml

Add the following lines to your `.gitlab-ci.yml` file to use the private key for authentication and deploy with rsync:

**Variables should be define in project settings**

```yaml
deploy:
  image: alpine
  script:
    - apk add --no-cache rsync openssh-client
    - mkdir -p ~/.ssh
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_ed25519
    - chmod 600 ~/.ssh/id_ed25519
    - rsync -avz --delete --exclude-from=.rsyncignore -e "ssh -o StrictHostKeyChecking=no" $LOCAL_PATH $SSH_USER@$SSH_HOST:$REMOTE_PATH
```

The `--exclude-from=.rsyncignore` option tells rsync to exclude the files and folders listed in the `.rsyncignore` file.

The `ssh -o StrictHostKeyChecking=no` option disables strict host key checking, which allows the deployment to proceed without manual intervention if the remote server's SSH host key changes.

The deployment script will use the private key stored in `SSH_PRIVATE_KEY` to authenticate with the remote server.

And that's it! 